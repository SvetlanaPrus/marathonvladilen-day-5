const startBtn = document.querySelector('#start');
const screens = document.querySelectorAll('.screen');
const timeList = document.querySelector('#time-list');
const timeEl = document.querySelector('#time');
const board = document.querySelector('#board');
let time = 0;
let score = 0;
const colors = ['#E070AC', '#1F4EE0', '#E6E622', '#6BE644', '#E02A12', '#7F6EE0', '#E05D2D', '#E027CD'];

/* pri nazatii knopki pojavljaetsja '#' v adres.stroke, t.k. v stroke line 13 (html) propisan href = '#'
* Chtobi etogo ne bilo, s pomoshju event mi eto ubiraem (stroka 7)
* **/

startBtn.addEventListener('click', (event) => {
    event.preventDefault();
    screens[0].classList.add('up');
})

timeList.addEventListener('click', event =>
{
    if (event.target.classList.contains('time-btn')){
        time = parseInt(event.target.getAttribute('data-time'));
        screens[1].classList.add('up');
        startGame();
    }
})

board.addEventListener('click', event => {
    if (event.target.classList.contains('circle')){
      score++;
      event.target.remove();
      createRandomCircle();
    }
})


function startGame(){
    // zadaem tajmer: cherez kazdij promezutok vremeni (1000 = 1s) chto mi zadaem, budet vipolnjat kakuju-to funkciju
    setInterval(decreaseTime, 1000);
    createRandomCircle();
    setTime(time);
}

function decreaseTime(){
    if (time === 0){
        finishGame();
    } else {
        let current = --time;
        setTime(current);
    }
}

function setTime(value){
    if (value < 10){
        timeEl.innerHTML = `00:0${value}`
    } else {
        timeEl.innerHTML = `00:${value}`
    }
}

function finishGame(){
    // timeEl.parentNode.remove(); // chtobi ne bilo skachka - dobavljaem class='hide'
    timeEl.parentNode.classList.add('hide');
    board.innerHTML = `<h1>Your scores: <span class="primary">${score}</span> </h1>`
}

function createRandomCircle(){
    const circle = document.createElement('div');
    const size = getRandomNumber(10, 60);
    const {width, height} = board.getBoundingClientRect(); // poluchit info o komponente
    const x = getRandomNumber(0, width - size);
    const y = getRandomNumber(0, height - size);
    const color = getRandomColor();  // '#E02A12'

    circle.classList.add('circle');
    circle.style.width = `${size}px`;
    circle.style.height = `${size}px`;
    circle.style.top = `${y}px`;
    circle.style.left = `${x}px`;
    circle.style.background = `linear-gradient(135deg, ${color} 0%, #E070AC 49%, #6BE644 100%)`;

    board.append(circle);
}

function getRandomNumber(min, max){
    return Math.round(Math.random() * (max-min) + min )
}

function getRandomColor(){
    const index = Math.floor(Math.random() * colors.length);
    return colors[index];
}
